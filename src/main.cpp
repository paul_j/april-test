/*-----------------------------------------
* @file main.cpp
* @brief
* @author Paul J
* @date Created: 16-04-2014
* @date Last Modified: Tue 09 Sep 2014 09:02:17 PM PDT
*-----------------------------------------*/

#include <iostream>
#include "config/CompileConfig.h"
#include "ecs/GameLoop.h"
#include "g2log/src/g2logworker.h"
#include <X11/Xlib.h>
#include "ecs/util/Units.h"

using namespace std;

int main(int argc, char* argv[]) {
    XInitThreads();
    g2LogWorker g2log(argv[0], "var/log/");
    g2::initializeLogging(&g2log);

    LOGF_IF(INFO, (argc < 2), "%s Version %d.%d\n",
                argv[0],
                VERSION_MAJOR,
                VERSION_MINOR);

    GameLoop gameLoop(60 /*FPS*/);
    gameLoop.start();
    return 1;
}
