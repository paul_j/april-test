/*-----------------------------------------
* @file KeyboardAffectedComponent.h
* @brief
* @author Paul J
* @date Created: 21-11-2014
* @date Last Modified: Wed 21 Nov 2014 10:46:10 PM PDT
*-----------------------------------------*/

#include "ecs/components/KeyboardAffectedComponent.h"

KeyboardAffectedComponent::KeyboardAffectedComponent(const component_id id, const entity_id parentEntity) :
    ConcreteComponent<KeyboardAffectedComponent>(id, parentEntity) { }

KeyboardAffectedComponent::~KeyboardAffectedComponent() { }

const component_id KeyboardAffectedComponent::create(const entity_id parentEntity) {
    const component_id id = getFreeId();
    storeComponent(KeyboardAffectedComponent(id, parentEntity));
    return id;
}
