
/*-----------------------------------------
* @file EventHandlerComponent.cpp
* @brief
* @author Paul J
* @date Created: 28-08-2014
* @date Last Modified: Wed 10 Sep 2014 10:45:39 PM PDT
*-----------------------------------------*/

#include "ecs/components/EventHandlerComponent.h"

EventHandlerComponent::EventHandlerComponent(const component_id id, const entity_id parentEntity)
        : ConcreteComponent<EventHandlerComponent>(id, parentEntity)
{
    events = std::make_shared<std::unordered_map<sf::Event::EventType, std::vector<sf::Event>, std::hash<int>>>();
}

EventHandlerComponent::~EventHandlerComponent()
{
}

const component_id EventHandlerComponent::create(const entity_id parentEntity) {
    const component_id id = getFreeId();
    storeComponent(EventHandlerComponent(id, parentEntity));
    return id;
}
