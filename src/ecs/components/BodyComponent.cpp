/*-----------------------------------------
* @file BodyComponent.cpp
* @brief
* @author Paul J
* @date Created: 12-04-2015
* @date Last Modified:
*-----------------------------------------*/

#include "ecs/components/BodyComponent.h"

BodyComponent::BodyComponent(const BodyComponent& other) : ConcreteComponent<BodyComponent>(other) {
    position = other.position;
    dimensions = other.dimensions;
    velocity = other.velocity;
    mass = other.mass;
    moveable = other.moveable;
    collidable = other.collidable;
}

const component_id BodyComponent::create(const std::pair<length_type, length_type> position,
                                         const std::pair<length_type, length_type> dimensions,
                                         const entity_id parentEntity) {
    auto id = getFreeId();
    storeComponent(
            BodyComponent(position, dimensions, std::make_pair(0.0 * meter_per_second, 0.0 * meter_per_second),
                          0.0 * kilogram, true, false, id, parentEntity));
    return id;
}

const component_id BodyComponent::create(const std::pair<length_type, length_type> position,
                                         const std::pair<length_type, length_type> dimensions,
                                         const std::pair<vel_type, vel_type> velocity, const mass_type mass,
                                         bool isMoveable, bool isCollidable, const entity_id parentEntity) {
    auto id = getFreeId();
    storeComponent(BodyComponent(position, dimensions, velocity, mass, isMoveable, isCollidable, id, parentEntity));
    return id;
}

std::pair<length_type, length_type> const &BodyComponent::getPosition() const {
    return position;
}

void BodyComponent::setPosition(const std::pair<length_type, length_type> position) {
    BodyComponent::position = position;
}

void BodyComponent::setPosition(const length_type x, const length_type y) {
    BodyComponent::position = std::make_pair(x, y);
}

void BodyComponent::move(const std::pair<length_type, length_type> offset) {
    setPosition(position.first + offset.first, position.second + offset.second);
}

void BodyComponent::move(const length_type x, const length_type y) {
    setPosition(position.first + x, position.second + y);
}

void BodyComponent::setDimensions(const std::pair<length_type, length_type> dimensions) {
    BodyComponent::dimensions = dimensions;
}

void BodyComponent::setDimensions(const length_type width, const length_type height) {
    dimensions = std::make_pair(width, height);
}

void BodyComponent::setVelocity(const std::pair<vel_type, vel_type> velocity) {
    BodyComponent::velocity = velocity;
}

void BodyComponent::setVelocity(const vel_type xVel, const vel_type yVel) {
    velocity = std::make_pair(xVel, yVel);
}

mass_type const &BodyComponent::getMass() const {
    return mass;
}

void BodyComponent::setMass(const mass_type mass) {
    BodyComponent::mass = mass;
}

bool BodyComponent::isMoveable() const {
    return moveable;
}

void BodyComponent::setMoveable(bool moveable) {
    BodyComponent::moveable = moveable;
}

bool BodyComponent::isCollidable() const {
    return collidable;
}

void BodyComponent::setCollidable(bool collidable) {
    BodyComponent::collidable = collidable;
}

bool BodyComponent::operator==(const BodyComponent &other) const {
    return position == other.position
            && dimensions == other.dimensions
            && velocity == other.velocity
            && mass == other.mass
            && moveable == other.moveable
            && collidable == other.collidable;
}

BodyComponent::BodyComponent(const std::pair<length_type, length_type> position,
                             const std::pair<length_type, length_type> dimensions,
                             const std::pair<vel_type, vel_type> velocity,
                             const mass_type mass,
                             bool isMoveable,
                             bool isCollidable,
                             const component_id id,
                             const entity_id parentEntity)
        : ConcreteComponent<BodyComponent>(id, parentEntity) {
    BodyComponent::position = position;
    BodyComponent::dimensions = dimensions;
    BodyComponent::velocity = velocity;
    BodyComponent::mass = mass;
    BodyComponent::moveable = isMoveable;
    BodyComponent::collidable = isCollidable;
}