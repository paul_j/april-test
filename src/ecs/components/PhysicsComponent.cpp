#include <bitset>
#include "ecs/components/PhysicsComponent.h"

PhysicsComponent::PhysicsComponent(const std::pair<vel_type, vel_type> velocity,
                                   const mass_type massInKg,
                                   const component_id id,
                                   const entity_id parentEntity)
    : PhysicsComponent(velocity, massInKg, true, true, id, parentEntity) {}

PhysicsComponent::PhysicsComponent(const std::pair<vel_type, vel_type> velocity,
                                   const mass_type massInKg,
                                   bool collidable,
                                   bool moveable,
                                   const component_id id,
                                   const entity_id parentEntity)
        : ConcreteComponent<PhysicsComponent>(id, parentEntity)
{
    PhysicsComponent::velocity = velocity;
    mass = massInKg;
    setCollidable(collidable);
    setMoveable(moveable);
}

PhysicsComponent::PhysicsComponent(PhysicsComponent const &other)
    : ConcreteComponent<PhysicsComponent>(other.componentId, other.entityId)
{
    velocity = other.velocity;
    mass = other.mass;
}

const component_id PhysicsComponent::create(const std::pair<vel_type, vel_type> velocity,
                                            const mass_type massInKg,
                                            const entity_id parentEntity)
{
    auto id = getFreeId();
    storeComponent(PhysicsComponent(velocity, massInKg, id, parentEntity));
    return id;
}

bool PhysicsComponent::operator==(PhysicsComponent const &other) const {
    return getVelocity() == other.getVelocity();
}

std::pair<vel_type, vel_type> const& PhysicsComponent::getVelocity() const {
    return velocity;
}

void PhysicsComponent::setVelocity(std::pair<vel_type, vel_type> velocity) {
    PhysicsComponent::velocity = velocity;
}

void PhysicsComponent::addVelocity(const std::pair<vel_type, vel_type> velocity) {
    PhysicsComponent::velocity.first += velocity.first;
    PhysicsComponent::velocity.second += velocity.second;
}

mass_type const& PhysicsComponent::getMass() const {
    return mass;
}

void PhysicsComponent::setMass(const mass_type mass) {

}

void PhysicsComponent::setMoveable(bool moveable) {
    flags[0] = moveable;
}

bool PhysicsComponent::isMoveable() const {
    return flags[0];
}

void PhysicsComponent::setCollidable(bool collidable) {
    flags[1] = collidable;
}

bool PhysicsComponent::isCollidable() const {
    return flags[1];
}
