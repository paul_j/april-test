#include <ecs/components/PositionComponent.h>

PositionComponent::PositionComponent(std::pair<double, double> position, const component_id id, const entity_id parentEntity)
        : ConcreteComponent<PositionComponent>(id, parentEntity) {
    PositionComponent::position = position;
}

PositionComponent::PositionComponent(PositionComponent const &other) :
    ConcreteComponent<PositionComponent>(other.componentId, other.entityId)
{
    position = other.position;
}

PositionComponent::~PositionComponent() { }

bool PositionComponent::operator==(PositionComponent const &other) const {
    return PositionComponent::getPosition() == other.getPosition();
}

std::pair<double, double> const &PositionComponent::getPosition() const {
    return position;
}

void PositionComponent::setPosition(const std::pair<double, double> position) {
    PositionComponent::position = position;
}

void PositionComponent::addOffset(const std::pair<double, double> offset) {
    return addOffset(offset.first, offset.second);
}

void PositionComponent::setPosition(const double x, const double y) {
    return setPosition(std::make_pair(x, y));
}

void PositionComponent::addOffset(const double x, const double y) {
    const std::pair<double, double> position = getPosition();
    return setPosition(std::make_pair(position.first + x, position.second + y));
}

std::pair<double, double> &PositionComponent::getPositionRef() {
    return position;
}

const component_id PositionComponent::create(std::pair<double, double> position, const entity_id parentEntity) {
    auto id = getFreeId();
    storeComponent(PositionComponent(position, id, parentEntity));
    return id;
}
