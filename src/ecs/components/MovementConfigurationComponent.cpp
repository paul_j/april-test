

#include "ecs/components/MovementConfigurationComponent.h"

MovementConfigurationComponent::MovementConfigurationComponent(
        const std::pair<accel_type, accel_type> acceleration,
        const std::pair<vel_type, vel_type> maxVelocity,
        const component_id id,
        const entity_id parentEntity)
        : ConcreteComponent<MovementConfigurationComponent>(id, parentEntity)
{
    MovementConfigurationComponent::acceleration = acceleration;
    MovementConfigurationComponent::maxVelocity = maxVelocity;
}

MovementConfigurationComponent::MovementConfigurationComponent(const MovementConfigurationComponent &other)
    : ConcreteComponent<MovementConfigurationComponent>(other.componentId, other.entityId)
{
    acceleration = other.getAcceleration();
    maxVelocity = other.getMaxVelocity();
}

const component_id MovementConfigurationComponent::create(const std::pair<accel_type, accel_type> acceleration,
                                                          const std::pair<vel_type, vel_type> maxVelocity,
                                                          const entity_id parentEntity)
{
    const component_id id = getFreeId();
    storeComponent(MovementConfigurationComponent(acceleration, maxVelocity, id, parentEntity));
    return id;
}

MovementConfigurationComponent::~MovementConfigurationComponent() { }

bool MovementConfigurationComponent::operator==(const MovementConfigurationComponent &other) const {
    return acceleration == other.getAcceleration() && maxVelocity == other.getMaxVelocity();
}

std::pair<accel_type, accel_type> const& MovementConfigurationComponent::getAcceleration() const {
    return acceleration;
}

void MovementConfigurationComponent::setAcceleration(const std::pair<accel_type, accel_type> acceleration) {
    MovementConfigurationComponent::acceleration = acceleration;
}

std::pair<vel_type, vel_type> const& MovementConfigurationComponent::getMaxVelocity() const {
    return maxVelocity;
}

void MovementConfigurationComponent::setMaxVelocity(const std::pair<vel_type, vel_type> maxVelocity) {
    MovementConfigurationComponent::maxVelocity = maxVelocity;
}


