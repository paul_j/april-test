/*-----------------------------------------
* @file DescriptionComponent.cpp
* @brief
* @author Paul J
* @date Created: 16-04-2014
* @date Last Modified: Wed 10 Sep 2014 09:48:44 PM PDT
*-----------------------------------------*/

#include "ecs/components/DescriptionComponent.h"

const component_id DescriptionComponent::create(const string& descriptionText, const entity_id parentEntity) {
    const component_id id = getFreeId();
    storeComponent(DescriptionComponent(descriptionText, id, parentEntity));
    return id;
}

/**
 * @brief Creates a new Description Component with the given text
 * @param _descriptionText 
 */
DescriptionComponent::DescriptionComponent(const string& _descriptionText, const component_id id, const entity_id parentEntity)
    : ConcreteComponent<DescriptionComponent>(id, parentEntity)
{
    descriptionText = _descriptionText;
}


/**
 * @brief Copies a given Description Component
 * @param other
 */
DescriptionComponent::DescriptionComponent(const DescriptionComponent& other)
    : ConcreteComponent<DescriptionComponent>(other.componentId, other.entityId)
{
    descriptionText = other.descriptionText;
}


DescriptionComponent::~DescriptionComponent() { }

bool DescriptionComponent::operator==(const DescriptionComponent& other) const {
    return other.descriptionText == descriptionText;
}

/**
 * @brief Sets the text to a given value
 *
 * @param _descriptionText
 */
void DescriptionComponent::setDescriptionText(const string& _descriptionText)
{
    descriptionText = _descriptionText;
}

/**
 * @brief Returns the configured description text
 *
 * @return The text configured for this Description Component
 */
const string& DescriptionComponent::getDescriptionText() const
{
    return descriptionText;
}

