#include "ecs/components/SpriteComponent.h"

SpriteComponent::SpriteComponent(sf::Sprite sprite, const component_id id, const entity_id parentEntity)
        : ConcreteComponent<SpriteComponent>(id, parentEntity), sprite(sprite) { }

SpriteComponent::SpriteComponent(const sf::Texture &texture,
                                const int widthInPixels,
                                const int heightInPixels,
                                const component_id id,
                                const entity_id parentEntity)
    : ConcreteComponent<SpriteComponent>(id, parentEntity)
{
    sprite = sf::Sprite(texture);
    setSpriteSize(widthInPixels, heightInPixels);
}

SpriteComponent::SpriteComponent(const SpriteComponent &other)
    : ConcreteComponent<SpriteComponent>(other.componentId, other.entityId), sprite(other.sprite) { }

SpriteComponent::~SpriteComponent() {
}

const component_id SpriteComponent::create(const sf::Sprite &sprite, const entity_id parentEntity) {
    auto id = getFreeId();
    storeComponent(SpriteComponent(sprite, id, parentEntity));
    return id;
}

const component_id SpriteComponent::create(const sf::Texture &texture,
                                            const int widthInPixels,
                                            const int heightInPixels,
                                            const entity_id parentEntity)
{
    auto id = getFreeId();
    storeComponent(SpriteComponent(texture, widthInPixels, heightInPixels, id, parentEntity));
    return id;
}

bool SpriteComponent::operator==(const SpriteComponent &other) const {
    return (this == &other);
}

sf::Sprite& SpriteComponent::getSpriteRef() {
    return sprite;
}

void SpriteComponent::setSprite(sf::Sprite sprite) {
    SpriteComponent::sprite = sprite;
}

void SpriteComponent::setTexture(const sf::Texture& texture) {
    sprite.setTexture(texture);
}

void SpriteComponent::setSpriteSize(const int width, const int height) {
    sf::FloatRect spriteSize = sprite.getGlobalBounds();
    sprite.scale(static_cast<double>(width) / spriteSize.width,
            static_cast<double>(height) / spriteSize.height);
}
