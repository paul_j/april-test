
/*-----------------------------------------
* @file GameLoop.cpp
* @brief
* @author Paul J
* @date Created: 19-07-2014
* @date Last Modified: Wed 10 Sep 2014 08:12:06 PM PDT
*-----------------------------------------*/

#include "ecs/GameLoop.h"
#include <thread>
#include "ecs/Entities.h"
#include <ecs/components/KeyboardAffectedComponent.h>
#include <ecs/components/MovementConfigurationComponent.h>
#include "ecs/components/PhysicsComponent.h"
#include <ecs/systems/DrawingSystem.h>
#include <ecs/components/SpriteComponent.h>
#include <ecs/components/PositionComponent.h>
#include <ecs/systems/RenderSystem.h>
#include <ecs/systems/pipeline/Pipeline.h>
#include <future>
#include <ecs/systems/pipeline/SerialPipeline.h>
#include <ecs/systems/CirclePointerSystem.h>
#include <ecs/components/BodyComponent.h>
#include "ecs/systems/pipeline/AsyncGroupPipeline.h"
#include "ecs/components/EventHandlerComponent.h"
#include "ecs/systems/EventPopulationSystem.h"
#include "ecs/systems/GravitySystem.h"
#include "ecs/systems/EchoSystem.h"
#include "ecs/GlobalState.h"

GameLoop::GameLoop(double desiredFps) {
    maxFps = desiredFps;

    std::shared_ptr<Pipeline> defaultPipeline = make_shared<SerialPipeline>();
    defaultPipeline->addSystem(make_shared<EventPopulationSystem>(world));
    //defaultPipeline->addSystem(make_shared<GravitySystem>(world));
    defaultPipeline->addSystem(make_shared<DrawingSystem>(world), {std::type_index(typeid(EventPopulationSystem))});
    defaultPipeline->addSystem(make_shared<RenderSystem>(world), {std::type_index(typeid(DrawingSystem))});

    world.setPipeline(defaultPipeline);

    Entity &inputHandler = world.getEntities()->createEntity();
    inputHandler.setComponent<EventHandlerComponent>();

    ecos::textureLoader.loadTexture("pikachu");
    const sf::Texture &pikachu = ecos::textureLoader.getTexture("pikachu");

    std::srand(std::chrono::system_clock::now().time_since_epoch().count());
    for (int i = 0; i < 100; i++) {
        Entity &testRectangle = world.getEntities()->createEntity();
        testRectangle.setComponent<KeyboardAffectedComponent>();
        testRectangle.setComponent<MovementConfigurationComponent>(
                std::make_pair(1.0 * units::meter_per_second_squared, 1.0 * units::meter_per_second_squared),
                std::make_pair(5.0 * units::meter_per_second, 10 * units::meter_per_second));
        testRectangle.setComponent<PhysicsComponent>(
                std::make_pair(0 * units::meter_per_second, 0.5 * units::meter_per_second),
                1.0 * units::kilogram);
        testRectangle.setComponent<BodyComponent>(
                std::make_pair((std::rand() % 60) * units::meter, (std::rand() % 60) * units::meter),
                std::make_pair(0.5 * units::meter, 0.5 * units::meter),
                std::make_pair(0.0 * units::meter_per_second, 0.0 * units::meter_per_second),
                5.0 * units::kilogram,
                true,
                true);

        testRectangle.setComponent<SpriteComponent>(pikachu, 5, 5);
        testRectangle.setComponent<PositionComponent>(std::make_pair(std::rand() % 600, std::rand() % 600));
    }

    Entity& testBlock = world.getEntities()->createEntity();
    //testBlock.setComponent<PhysicsComponent>(std::make_pair(0,0), 1, true, false);
}

GameLoop::GameLoop(const GameLoop& other)
{
    world = other.world;
}

GameLoop::~GameLoop() { }

void GameLoop::start()
{
    typedef std::chrono::duration<long long int, std::nano> ns;
    std::chrono::duration<double, std::milli> desiredDurationOfTick =
        std::chrono::duration_cast<std::chrono::duration<double, std::milli>>(std::chrono::nanoseconds( 1000000000 / 60));
    std::chrono::duration<double, std::milli> howFarBehindSchedule =
        std::chrono::duration_cast<std::chrono::duration<double, std::milli>>(std::chrono::nanoseconds::zero());
    std::chrono::duration<double, std::milli> timeTaken; //should update with each tick

    while (!world.shouldExitProgram()) {
        auto startTime = std::chrono::system_clock::now();
        world.step();

        if (howFarBehindSchedule > std::chrono::milliseconds(3)) {
            LOGF(INFO, "%8.4f ns behind schedule, skipping redraw", howFarBehindSchedule.count());
        } else {
            world.redraw();
        }

        auto endTime = std::chrono::system_clock::now();
        timeTaken = std::chrono::duration_cast<std::chrono::duration<double, std::milli>>(endTime - startTime);
        LOGF(INFO, "%8.4f ms taken", timeTaken.count());

        /* If we took too long, mark that we're behind schedule (to skip
         * redraws until we're better), and kick off the next step immediately.
         * Otherwise, we finished early! Wait until the desired framerate is reached,
         * then go to the next step. 
         */
        howFarBehindSchedule += (timeTaken - desiredDurationOfTick);
        if (howFarBehindSchedule < std::chrono::nanoseconds::zero()) {
            std::this_thread::sleep_for(-howFarBehindSchedule);
            howFarBehindSchedule = std::chrono::nanoseconds::zero();
        }

        ns tickDuration = std::chrono::system_clock::now() - startTime;
        world.setTickDuration(tickDuration);
    }

    world.exitProgram();
}
