#include <iostream>
#include <fstream>
#include "ecs/util/TextureLoader.h"
#include "ecs/util/ConfigConstants.h"
#include "g2log/src/g2log.h"
#include "rapidjson/filereadstream.h"

#define DEFAULT_RAPIDJSON_FLAGS 0

const sf::Texture TextureLoader::INVALID_TEXTURE;
const static char* PATH_KEY = "path";
const static char* TYPE_KEY = "type";
const static char* X_OFFSET_KEY = "xOffset";
const static char* Y_OFFSET_KEY = "yOffset";
const static char* WIDTH_KEY = "width";
const static char* HEIGHT_KEY = "height";

static int entryOrDefault(const rapidjson::Value& entry, const char* key, int defaultInt);

TextureLoader::TextureLoader() {
    std::stringstream ss;
    ss << ecos::BASE_DIR << ecos::CONFIG_DIR << ecos::TEXTURE_FILENAME;
    std::string textureString = ss.str();
    const char* path = textureString.c_str();
    textureFile = fopen(path, "rb");
    if (textureFile == NULL) {
        LOGF(INFO, "ERROR: Failed to load texture config '%s'", path);
        std::cout << "Error opening texture config: see logs for more details" << std::endl;
    } else {
        rapidjson::FileReadStream jsonStream(textureFile, readBuffer, sizeof(readBuffer));
        textureJson.ParseStream<DEFAULT_RAPIDJSON_FLAGS, rapidjson::UTF8<>, rapidjson::FileReadStream>(jsonStream);
    }
}

TextureLoader::~TextureLoader() {
    if (textureFile != NULL) {
        fclose(textureFile);
    }
}

void TextureLoader::loadTexture(texture_handle textureId) {
    loadTexture(textureId.c_str());
}

void TextureLoader::loadTexture(const char* const textureId) {
    if (textureJson.HasMember(textureId)) {
        const rapidjson::Value& textureElement = textureJson[textureId];
        if (!textureElement.HasMember(PATH_KEY) || !textureElement.HasMember(TYPE_KEY)) {
            //If the element is present but is missing 'path' or 'type', it's still invalid
            LOGF(INFO, "ERROR: Failed to load texture '%s', was missing '%s' or '%s'", textureId, PATH_KEY, TYPE_KEY);
        } else {
            addTextureToMap(textureId, textureElement);
        }
    } else {
        //If we didn't even find a config entry with the given id, error
        LOGF(INFO, "ERROR: Failed to load texture '%s', was missing id of that name", textureId);
    }
}

void TextureLoader::removeTexture(texture_handle textureHandle) {
    textures.erase(textureHandle);
}

void TextureLoader::clear() {
    textures.clear();
}

void TextureLoader::addTextureToMap(const std::string& textureId, const rapidjson::Value &entry) {
    std::string fullTexturePath = std::string(ecos::BASE_DIR) + std::string(ecos::ASSET_DIR) + entry[PATH_KEY].GetString();
    std::string textureType = entry[TYPE_KEY].GetString();

    //If the texture json config has offsets specified, go ahead and use them (so long
    // as they're both positive integers)
    int xOffset = entryOrDefault(entry, X_OFFSET_KEY, 0);
    int yOffset = entryOrDefault(entry, Y_OFFSET_KEY, 0);
    int width   = entryOrDefault(entry, WIDTH_KEY, 0);
    int height  = entryOrDefault(entry, HEIGHT_KEY, 0);

    if (textureType == "file") {
        sf::Texture texture;

        if (xOffset > 0 && yOffset > 0 && width > 0 && height > 0)
            texture.loadFromFile(fullTexturePath, sf::IntRect(xOffset, yOffset, width, height));
        else
            texture.loadFromFile(fullTexturePath);

        textures[textureId] = texture;

    } else {
        LOGF(INFO, "ERROR: Unsupported texture type '%s' in config", textureType.c_str());
    }
}

//Helper function
static int entryOrDefault(const rapidjson::Value& entry, const char* key, int defaultInt) {
    if (entry.HasMember(key)) {
        const rapidjson::Value& value = entry[key];
        if (value.IsInt()) return value.GetInt();
    }

    return defaultInt;
}

const sf::Texture& TextureLoader::getTexture(texture_handle textureHandle) {
    if (textures.find(textureHandle) == textures.end()) {
        return TextureLoader::INVALID_TEXTURE;
    } else {
        return textures[textureHandle];
    }
}

const bool TextureLoader::hasTexture(texture_handle textureHandle) {
    return (textures.find(textureHandle) != textures.end());
}