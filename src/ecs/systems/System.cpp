
/*-----------------------------------------
* @file System.cpp
* @brief
* @author Paul J
* @date Created: 22-04-2014
* @date Last Modified: Thu 28 Aug 2014 10:47:02 PM PDT
*-----------------------------------------*/

#include "ecs/systems/System.h"
#include "ecs/Entities.h"

std::mutex System::windowMutex;

System::System(World& world) : referencedWorld(world) { }

System::System(const System& other) : referencedWorld(other.referencedWorld) 
{
    requirements = other.requirements;
    trackedEntities = other.trackedEntities;
}

System::~System() { }

bool System::shouldTrack(entity_id id) const {
    const Entity& entity = referencedWorld.getEntities()->get(id);
    return shouldTrack(entity);
}

bool System::shouldTrack(const Entity& entity) const
{
    for (std::type_index requirement : requirements) {
        if (!entity.hasComponent(requirement)) {
            
            return false; //Our entity didn't meet all the reqs
        }
    }
    return true;
}

/**
 * Adds a given entity to this system's list of entities to track
 * @brief Tracks a given entity in this system
 *
 * @param id The id of the entity to track
 */
void System::trackEntity(entity_id id)
{
    if (!std::binary_search(trackedEntities.begin(), trackedEntities.end(), id)){
        auto insertionPoint = std::lower_bound(trackedEntities.begin(), trackedEntities.end(), id);
        trackedEntities.insert(insertionPoint, id);
    }
}


/**
 * Removes the entity with the given entity_id from the system's list
 * of tracked entities
 * @brief Untracks the given entity in this system
 *
 * @param id The id of the entity to stop tracking
 */
void System::untrackEntity(entity_id id) {

    auto entryIter = std::lower_bound(trackedEntities.begin(), trackedEntities.end(), id);
    //We are guaranteeing in our track function that only one instance of
    // each id should be tracked. As such, only delete the single element we find
    trackedEntities.erase(entryIter);
}

