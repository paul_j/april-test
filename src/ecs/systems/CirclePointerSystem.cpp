
/*-----------------------------------------
* @file CirclePointerSystem.cpp
* @brief
* @author Paul J
* @date Created: 10-09-2014
* @date Last Modified: Wed 10 Sep 2014 10:05:35 PM PDT
*-----------------------------------------*/

#include "ecs/systems/CirclePointerSystem.h"
#include "ecs/components/EventHandlerComponent.h"
#include <iostream>
#include <g2log/src/g2log.h>

CirclePointerSystem::CirclePointerSystem(World& world) : System(world)
{
    requirements.push_back(std::type_index(typeid(EventHandlerComponent)));
}

CirclePointerSystem::CirclePointerSystem(const CirclePointerSystem& other) :
    System(other) { }

CirclePointerSystem::~CirclePointerSystem() { }

void CirclePointerSystem::execute()
{
    auto now = std::chrono::system_clock::now();

    for (EventHandlerComponent& eventHandler : EventHandlerComponent::getSortedComponents()) {
        std::vector<sf::Event> clickEvents = (*eventHandler.events)[sf::Event::MouseButtonPressed];

        for( const sf::Event& event : clickEvents) {
            sf::CircleShape shape(50);
            shape.setFillColor(sf::Color(150, 50, 250));
            shape.setPosition(event.mouseButton.x - 50, event.mouseButton.y - 50);

            referencedWorld.getWindow()->draw(shape);
            std::cout << "Drew shape" << std::endl;
        }
    }

    LOGF(INFO, "Circle Pointer System time: %ld ns", std::chrono::duration_cast<
            std::chrono::nanoseconds>(std::chrono::system_clock::now() - now).count());
}

