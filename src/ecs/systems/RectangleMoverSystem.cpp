/*-----------------------------------------
* @file RectangleMoverSystem.cpp
* @brief
* @author Paul J
* @date Created: 21-11-2014
* @date Last Modified:
*-----------------------------------------*/

#include <iostream>
#include "ecs/components/PhysicsComponent.h"
#include "ecs/components/KeyboardAffectedComponent.h"
#include "ecs/components/MovementConfigurationComponent.h"
#include "ecs/systems/RectangleMoverSystem.h"

RectangleMoverSystem::RectangleMoverSystem(World &world) : System(world) {
    requirements.push_back(std::type_index(typeid(KeyboardAffectedComponent)));
    requirements.push_back(std::type_index(typeid(MovementConfigurationComponent)));
    requirements.push_back(std::type_index(typeid(PhysicsComponent)));
}

RectangleMoverSystem::RectangleMoverSystem(const RectangleMoverSystem &other) : System(other) { }

RectangleMoverSystem::~RectangleMoverSystem() { }

void RectangleMoverSystem::execute()
{
    auto now = std::chrono::system_clock::now();

    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left) ||
            sf::Keyboard::isKeyPressed(sf::Keyboard::Right) ||
            sf::Keyboard::isKeyPressed(sf::Keyboard::Up) ||
            sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
    {
        const ns tickTime = referencedWorld.getTickDuration();
        dSeconds accelerationDuration = std::chrono::duration_cast<dSeconds>(tickTime);
        double secondsToAccelerateFor = accelerationDuration.count();
        const bool isLeft  = sf::Keyboard::isKeyPressed(sf::Keyboard::Left);
        const bool isRight = sf::Keyboard::isKeyPressed(sf::Keyboard::Right);
        const bool isUp    = sf::Keyboard::isKeyPressed(sf::Keyboard::Up);
        const bool isDown  = sf::Keyboard::isKeyPressed(sf::Keyboard::Down);

        ComponentIterator<MovementConfigurationComponent, PhysicsComponent> componentIterator;
        for (;!componentIterator.isFinished(); ++componentIterator) {
            /*auto componentTuple = componentIterator.get();
            MovementConfigurationComponent& movementConfigComponent = std::get<0>(componentTuple);
            PhysicsComponent & velocityComponent = std::get<1>(componentTuple);

            std::pair<double, double> acceleration = movementConfigComponent.getAcceleration();
            std::pair<double, double> currentVelocity = velocityComponent.getVelocity();
            std::pair<double, double> maxVelocity = movementConfigComponent.getMaxVelocity();

            double xAccelerationThisTick = acceleration.first * secondsToAccelerateFor * (isLeft? -1 : isRight? 1 : 0);
            double yAccelerationThisTick = acceleration.second * secondsToAccelerateFor * (isDown? -1 : isUp? 1 : 0);
            double newXVelocity = currentVelocity.first + xAccelerationThisTick;
            double newYVelocity = currentVelocity.second + yAccelerationThisTick;

            velocityComponent.setVelocity(std::min(maxVelocity.first, newXVelocity),
                                          std::min(maxVelocity.second, newYVelocity));
                                          */
        }
    }

    LOGF(INFO, "Rectangle Mover System time: %ld ns", std::chrono::duration_cast<
            std::chrono::nanoseconds>(std::chrono::system_clock::now() - now).count());

}
