#include <ecs/components/PhysicsComponent.h>
#include <ecs/components/PositionComponent.h>
#include "ecs/systems/GravitySystem.h"

GravitySystem::GravitySystem(World& world) : System(world) { }

GravitySystem::GravitySystem(const GravitySystem &other) : System(other) { }

void GravitySystem::execute() {
    units::time_type secondsElapsed = (1.0 * std::chrono::seconds(1) / referencedWorld.getTickDuration()) * units::second;
    for (PhysicsComponent& physics : PhysicsComponent::getSortedComponents()) {
        auto&& gravityVelocityChange = 9.8 * units::meter_per_second_squared * secondsElapsed;
        physics.addVelocity(std::make_pair(0 * units::meter_per_second, gravityVelocityChange));
    }

    ComponentIterator<PositionComponent, PhysicsComponent> movementIterator;
    for (;!movementIterator.isFinished(); ++movementIterator) {
        auto positionPhysicsPair = movementIterator.get();
        PositionComponent& position = std::get<0>(positionPhysicsPair);
        PhysicsComponent& physics = std::get<1>(positionPhysicsPair);

        auto& velocity = physics.getVelocity();
        units::length_type horizontalMotion = velocity.first * secondsElapsed;
        units::length_type verticalMotion = velocity.second * secondsElapsed;
        position.addOffset(std::min(horizontalMotion.value() * 0.000001, 20.0), std::min(verticalMotion.value() * 0.000001, 20.0));
    }
}
