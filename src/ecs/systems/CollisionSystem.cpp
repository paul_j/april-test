//
// Created by paul on 3/24/15.
//

#include <ecs/components/PhysicsComponent.h>
#include <ecs/components/PositionComponent.h>
#include "ecs/systems/CollisionSystem.h"

CollisionSystem::CollisionSystem(World &world) : System(world) {}

CollisionSystem::CollisionSystem(const CollisionSystem &other) : System(other) { }

void CollisionSystem::execute() {
    ComponentIterator<PositionComponent, PhysicsComponent> outerIterator;

    for (int i = 0; !outerIterator.isFinished(); ++outerIterator, ++i) {
        ComponentIterator<PositionComponent, PhysicsComponent> innerIterator;
        for (int j = 0; !innerIterator.isFinished(); ++innerIterator, ++j) {
            auto entityOne = outerIterator.get();
            auto entityTwo = innerIterator.get();


        }
    }

}
