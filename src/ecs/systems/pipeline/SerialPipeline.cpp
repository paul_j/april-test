#include "ecs/systems/pipeline/SerialPipeline.h"

const static std::vector<std::type_info> EMPTY_VECTOR;


SerialPipeline::SerialPipeline() : Pipeline() { }

SerialPipeline::~SerialPipeline() { }

const void SerialPipeline::executeSystems() {
    for (std::shared_ptr<System> system : systems) {
        system->execute();
    }
}

void SerialPipeline::addSystem(std::shared_ptr<System> system) {
    systems.insert(systems.begin(), system);
}

void SerialPipeline::addSystem(std::shared_ptr<System> system, const std::initializer_list<std::type_index> dependencies) {
    unsigned short numDependenciesMet = 0;

    for (std::vector<std::shared_ptr<System>>::iterator iter = systems.begin(); iter != systems.end(); iter++) {
        std::type_index iterSystemType = std::type_index(typeid(**iter));
        if (std::find(dependencies.begin(), dependencies.end(), iterSystemType) != dependencies.end()) {
            ++numDependenciesMet;
        }

        if (numDependenciesMet == dependencies.size()) {
            systems.insert(iter + 1, system);
            return;
        }
    }

    systems.push_back(system);
}

//void SerialPipeline::removeSystem(std::type_info systemType) {
//    systems.erase(std::remove_if(systems.begin(), systems.end(),
//            [&systemType](std::shared_ptr<System> s) {return typeid(s) == systemType;}), systems.end());
//}

const std::vector<std::shared_ptr<System>> &SerialPipeline::getSystems() {
    return systems;
}

