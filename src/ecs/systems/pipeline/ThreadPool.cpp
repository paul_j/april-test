#include "ecs/systems/pipeline/ThreadPool.h"
#include <chrono>
#include <iostream>

static std::mutex taskLock;

ThreadPool::ThreadPool(const int size) {
    for (int i = 0; i < size; i++) {
        threadPool.push_back(std::thread([this]{processEvents();}));
    }
}

ThreadPool::~ThreadPool() {
    shouldEnd = true;
    for (std::thread& thread : threadPool) {
        thread.join();
    }
}

void ThreadPool::execute(std::shared_future<void> deferredExecution) {
    tasks.push_back(deferredExecution);
}

void ThreadPool::processEvents() {
    bool shouldSleep;
    std::shared_future<void> nextTask;

    while (!shouldEnd) {
        shouldSleep = false;

        {
            std::lock_guard<std::mutex> lock(taskLock);
            if (tasks.empty()) {
                shouldSleep = true;
            } else {
                nextTask = tasks.front();
                tasks.pop_front();
            }
        }

        if (shouldSleep) {
            std::this_thread::sleep_for(std::chrono::microseconds(500));
        } else {
            nextTask.wait();
        }
    }
}

void ThreadPool::wait() {
    while (!tasks.empty()) {
        std::this_thread::sleep_for(std::chrono::microseconds(1000));
    }
}