#include "ecs/systems/pipeline/AsyncGroupPipeline.h"
#include <bitset>
#include <future>
#include <iostream>

AsyncGroupPipeline::AsyncGroupPipeline() : Pipeline() , threadPool(4) { }

AsyncGroupPipeline::~AsyncGroupPipeline() { }

constexpr const int MAX_NUM_DEPENDENCIES = 20;

const void AsyncGroupPipeline::executeSystems() {
    for (auto executionGroup : systemGroups) {
        std::vector<std::future<void>> futures(executionGroup.size());
        for (std::shared_ptr<System> systemPtr : executionGroup) {
            threadPool.execute(std::async(std::launch::deferred, &System::execute, &(*systemPtr)));
        }

        threadPool.wait();
    }
}

void AsyncGroupPipeline::addSystem(std::shared_ptr<System> const system) {
    if (systemGroups.size() < 1) {
        std::vector<std::shared_ptr<System>> noDependencySystems;
        noDependencySystems.push_back(system);
        systemGroups.push_back(noDependencySystems);
    } else {
        systemGroups[0].push_back(system);
    }

    allSystems.push_back(system);
}

void AsyncGroupPipeline::addSystem(std::shared_ptr<System> const newSystem, const std::initializer_list<std::type_index> dependencies) {
    std::bitset<MAX_NUM_DEPENDENCIES> dependenciesMet;
    std::vector<std::type_index> dependencyVector(dependencies);

    auto groupIter = systemGroups.begin();
    while (dependenciesMet.count() != dependencyVector.size() && groupIter != systemGroups.end()) {
        for (std::shared_ptr<System> systemPtr : *groupIter) {
            std::type_index iterSystemType = std::type_index(typeid(*systemPtr));
            for (unsigned short i = 0; i < dependencyVector.size(); i++) {
                if (dependencyVector[i] == iterSystemType) {
                    dependenciesMet[i] = 1;
                }
            }
        }

        groupIter++;
    }

    if (groupIter == systemGroups.end()) {
        std::vector<std::shared_ptr<System>> newSystemVector({newSystem});
        systemGroups.insert(groupIter, newSystemVector);
    } else {
        groupIter->push_back(newSystem);
    }
    allSystems.push_back(newSystem);
}

const std::vector<std::shared_ptr<System>> &AsyncGroupPipeline::getSystems() {
    return allSystems;
}
