#include <ecs/components/SpriteComponent.h>
#include <ecs/components/PositionComponent.h>
#include "ecs/systems/RenderSystem.h"

RenderSystem::RenderSystem(World& world) : System(world), vertexArray(sf::PrimitiveType::Quads){
    requirements.push_back(std::type_index(typeid(SpriteComponent)));
}

RenderSystem::RenderSystem(const RenderSystem &other) : System(other.referencedWorld) { }

RenderSystem::~RenderSystem() { }

void RenderSystem::execute() {
    auto now = std::chrono::system_clock::now();

    sf::RenderWindow& window = *(referencedWorld.getWindow());
    std::unordered_map<const sf::Texture*, std::vector<sf::Sprite>> textureMap;

    window.clear(sf::Color::Black);

    ComponentIterator<SpriteComponent, PositionComponent> componentIterator;
    for (; !componentIterator.isFinished(); ++componentIterator) {
        auto componentTuple = componentIterator.get();
        const sf::Sprite& sprite = std::get<0>(componentTuple).getSpriteRef();
        const sf::Texture* texture = sprite.getTexture();

        textureMap[texture].push_back(sprite);
    }

    for (auto entry : textureMap) {
        std::vector<sf::Sprite> sprites = entry.second;
        const sf::Texture* texture = entry.first;

        for (sf::Sprite sprite : sprites) {
            auto textureCoords = sprite.getTextureRect();
            auto position = sprite.getPosition();
            auto bounds = sprite.getLocalBounds();

            vertexArray.append(sf::Vertex(position + sf::Vector2f(bounds.left, bounds.top),
                    sf::Vector2f(textureCoords.left, textureCoords.top)));
            vertexArray.append(sf::Vertex(position + sf::Vector2f(bounds.left + 0.01 * bounds.width, bounds.top),
                    sf::Vector2f(textureCoords.left + textureCoords.width, textureCoords.top)));
            vertexArray.append(sf::Vertex(position + sf::Vector2f(bounds.left + 0.01 * bounds.width, bounds.top + 0.01 * bounds.height),
                    sf::Vector2f(textureCoords.left + textureCoords.width, textureCoords.top + textureCoords.height)));
            vertexArray.append(sf::Vertex(position + sf::Vector2f(bounds.left, bounds.top + 0.01 * bounds.height),
                    sf::Vector2f(textureCoords.left, textureCoords.top + textureCoords.height)));
        }
        sf::RenderStates renderStates;
        renderStates.texture = texture;
        window.draw(vertexArray, renderStates);
    }


    window.display();
    vertexArray.clear();

    LOGF(INFO, "Render System time: %ld ns", std::chrono::duration_cast<
            std::chrono::nanoseconds>(std::chrono::system_clock::now() - now).count());
}
