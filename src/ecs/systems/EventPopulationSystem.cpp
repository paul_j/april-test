
/*-----------------------------------------
* @file EventPopulationSystem.cpp
* @brief
* @author Paul J
* @date Created: 28-08-2014
* @date Last Modified: Wed 10 Sep 2014 10:01:57 PM PDT
*-----------------------------------------*/

#include "ecs/systems/EventPopulationSystem.h"
#include "ecs/components/EventHandlerComponent.h"
#include <g2log/src/g2log.h>

EventPopulationSystem::EventPopulationSystem(World& world) : System(world)
{
    requirements.push_back(std::type_index(typeid(EventHandlerComponent)));
}

EventPopulationSystem::EventPopulationSystem(const EventPopulationSystem& other) : System(other) { }

EventPopulationSystem::~EventPopulationSystem() { }

void EventPopulationSystem::execute()
{
    auto now = std::chrono::system_clock::now();

    sf::Event event;
    std::unordered_map<sf::Event::EventType, std::vector<sf::Event>, std::hash<int> > eventMap;

    {
        std::lock_guard<std::mutex> lock(System::windowMutex);
        sf::RenderWindow &windowRef = *referencedWorld.getWindow();
        windowRef.setActive(true);
        while (windowRef.pollEvent(event)) {
            sf::Event eventCopy(event);
            eventMap[event.type].push_back(eventCopy);
        }

        windowRef.setActive(false);
    }

    auto sharedEventPtr =
            std::make_shared<std::unordered_map<sf::Event::EventType, std::vector<sf::Event>, std::hash<int>>>(eventMap);

    for (EventHandlerComponent& eventHandlerComponent : EventHandlerComponent::getSortedComponents()) {
        eventHandlerComponent.events = sharedEventPtr;
    }

    if (eventMap[sf::Event::EventType::Closed].size() > 0) {
        referencedWorld.exitProgram();
    }

    LOGF(INFO, "Event Population System time: %ld ns", std::chrono::duration_cast<
            std::chrono::nanoseconds>(std::chrono::system_clock::now() - now).count());
}
