
/*-----------------------------------------
* @file EchoSystem.cpp
* @brief
* @author Paul J
* @date Created: 28-08-2014
* @date Last Modified: Wed 10 Sep 2014 10:05:13 PM PDT
*-----------------------------------------*/

#include "ecs/systems/EchoSystem.h"
#include "ecs/components/EventHandlerComponent.h"
#include <iostream>
#include <g2log/src/g2log.h>

EchoSystem::EchoSystem(World& world) : System(world)
{
    requirements.push_back(std::type_index(typeid(EventHandlerComponent)));
}

EchoSystem::EchoSystem(const EchoSystem& other) : System(other) { }

EchoSystem::~EchoSystem() { }

void EchoSystem::execute()
{
    auto now = std::chrono::system_clock::now();

    for (const EventHandlerComponent& eventHandlerComponent : EventHandlerComponent::getSortedComponents()) {

        auto eventMap = eventHandlerComponent.events;
        std::vector<sf::Event> textEnteredEvents = (*eventMap)[sf::Event::TextEntered];
        for (sf::Event event : textEnteredEvents) {
            std::cout << static_cast<char>(event.text.unicode) << std::endl;
        }
    }

    LOGF(INFO, "Echo System time: %ld ns", std::chrono::duration_cast<
            std::chrono::nanoseconds>(std::chrono::system_clock::now() - now).count());
}

