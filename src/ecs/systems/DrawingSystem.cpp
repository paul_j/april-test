#include <ecs/components/SpriteComponent.h>
#include <ecs/components/KeyboardAffectedComponent.h>
#include <ecs/components/BodyComponent.h>
#include "ecs/systems/DrawingSystem.h"
#include "ecs/GlobalState.h"

double DrawingSystem::pixelsPerMeter = 10;

DrawingSystem::DrawingSystem(World &world) : System(world) {
    requirements.push_back(std::type_index(typeid(SpriteComponent)));
    requirements.push_back(std::type_index(typeid(BodyComponent)));
    requirements.push_back(std::type_index(typeid(KeyboardAffectedComponent)));
}

DrawingSystem::DrawingSystem(const DrawingSystem &other) :
        System(other.referencedWorld) { }

DrawingSystem::~DrawingSystem() { }

void DrawingSystem::execute()
{
    auto now = std::chrono::system_clock::now();

    bool isUp = false;
    bool isLeft = false;
    bool isRight = false;
    bool isDown = false;
    bool deleteOne = false;
    bool makeOne = false;

    if (sf::Keyboard::isKeyPressed(sf::Keyboard::W))
        isUp = true;
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::A))
        isLeft = true;
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::S))
        isDown = true;
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::D))
        isRight = true;

    ComponentIterator<SpriteComponent, BodyComponent, KeyboardAffectedComponent> componentIterator;
    for (;!componentIterator.isFinished(); ++componentIterator) {
        auto componentTuple = componentIterator.get();
        auto& spriteComponent = std::get<0>(componentTuple);
        auto& bodyComponent = std::get<1>(componentTuple);
        auto& keyboardAffectedComponent = std::get<2>(componentTuple);

        sf::Sprite& sprite = spriteComponent.getSpriteRef();
        const std::pair<length_type, length_type>& position = bodyComponent.getPosition();
        sprite.setPosition(position.first.value() * getPixelsPerMeter(),
                           position.second.value() * getPixelsPerMeter());

        if (isUp)
            bodyComponent.move(0 * units::meter, -0.01 * units::meter);
        if (isLeft)
            bodyComponent.move(-0.01 * units::meter, 0 * units::meter);
        if (isRight)
            bodyComponent.move(0.01 * units::meter, 0 * units::meter);
        if (isDown)
            bodyComponent.move(0 * units::meter, 0.01 * units::meter);

    }

    LOGF(INFO, "Drawing System time: %ld ns", std::chrono::duration_cast<
            std::chrono::nanoseconds>(std::chrono::system_clock::now() - now).count());
}

const double DrawingSystem::getPixelsPerMeter() {
    return pixelsPerMeter;
}

void DrawingSystem::setPixelsPerMeter(const double ppm) {

}
