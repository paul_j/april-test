
/*-----------------------------------------
* @file DescriptionSystem.cpp
* @brief
* @author Paul J
* @date Created: 17-07-2014
* @date Last Modified: Wed 10 Sep 2014 09:04:25 PM PDT
*-----------------------------------------*/

#include <iostream>
#include "ecs/systems/DescriptionSystem.h"
#include "ecs/components/DescriptionComponent.h"
#include <g2log/src/g2log.h>

DescriptionSystem::DescriptionSystem(World& world) : System(world) {
    requirements.push_back(std::type_index(typeid(DescriptionComponent)));
}


DescriptionSystem::DescriptionSystem(const DescriptionSystem& other) : System(other) { }

DescriptionSystem::~DescriptionSystem() { }

void DescriptionSystem::execute()
{
    auto now = std::chrono::system_clock::now();

    for (DescriptionComponent& descriptionComponent : DescriptionComponent::getSortedComponents()) {
        cout << "Entity " << descriptionComponent.getEntityId() << " is described as '" <<
                descriptionComponent.getDescriptionText() << endl;
    }

    LOGF(INFO, "Description System time: %ld ns", std::chrono::duration_cast<
            std::chrono::nanoseconds>(std::chrono::system_clock::now() - now).count());
}



