/*-----------------------------------------
* @file Entities.cpp
* @brief
* @author Paul J
* @date Created: 17-04-2014
* @date Last Modified: Tue 26 Aug 2014 07:49:37 PM PDT
*-----------------------------------------*/

#include <stdexcept>
#include <assert.h>
#include "ecs/systems/System.h"
#include "ecs/Entities.h"
#include "ecs/systems/pipeline/Pipeline.h"

Entities::Entities(World& world)
    : referencedWorld(world)
{
    nextEntityId = 0;
}

Entities::Entities(const Entities& other)
    : referencedWorld(other.referencedWorld)
{
    entities = other.entities;
    modifiedEntities = other.modifiedEntities;
    nextEntityId = other.nextEntityId;
    freedEntityIds = other.freedEntityIds;
}

Entities& Entities::operator= (const Entities& other)
{
    entities = other.entities;
    modifiedEntities = other.modifiedEntities;
    nextEntityId = other.nextEntityId;
    freedEntityIds = other.freedEntityIds;
    return *this;
}

Entities::~Entities() { }

Entity& Entities::get(entity_id id)
{
    return entities.at(id);
}

Entity& Entities::operator[](entity_id id) {
    return entities[id];
}

Entity& Entities::createEntity()
{
    entity_id id = getFreeEntityId();

    if (id == entities.size()) {
        entities.emplace_back(*this, id);
    } else {
        entities[id] = Entity(*this, id);
    }

    for (std::shared_ptr<System> system : referencedWorld.getPipeline()->getSystems()) {
        if (system->shouldTrack(id)) {
            system->trackEntity(id);
        }
    }

    return entities.at(id);
}

void Entities::deleteEntity(entity_id id)
{
    for (std::shared_ptr<System> system : referencedWorld.getPipeline()->getSystems()) {
        system->untrackEntity(id);
    }

    entities[id].isDeleted = true;
    freedEntityIds.push_back(id);
}

void Entities::updateTrackedEntitiesInSystems() {

    for (entity_id id : modifiedEntities.getUpdatedEntityIds()) {
        for (std::shared_ptr<System> system : referencedWorld.getPipeline()->getSystems()) {
            system->untrackEntity(id);
            if (system->shouldTrack(id)) {
                system->trackEntity(id);
            } 
        }
    }
    modifiedEntities.clear();
}

const entity_id Entities::getFreeEntityId() {
    if (freedEntityIds.empty()) {
        assert(nextEntityId == entities.size());
        return nextEntityId++;
    } else {
        auto freeId = freedEntityIds.back();
        freedEntityIds.pop_back();
        return freeId;
    }
}
