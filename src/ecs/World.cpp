
/*-----------------------------------------
* @file World.cpp
* @brief
* @author Paul J
* @date Created: 17-07-2014
* @date Last Modified: Wed 10 Sep 2014 08:17:20 PM PDT
*-----------------------------------------*/

#include <ecs/systems/pipeline/SerialPipeline.h>
#include "ecs/Entities.h"
#include "g2log/src/g2log.h"

World::World()
{
    window = std::make_shared<sf::RenderWindow>(sf::VideoMode(600,400), "My Window");
    entities = std::make_shared<Entities>(*this);
    pipeline = std::make_shared<SerialPipeline>();
}

World::World(const World& other)
{
    operator=(other);
}

World& World::operator=(const World& other)
{
    entities = std::make_shared<Entities>(*(other.entities));
    pipeline = other.pipeline;
    //We can go ahead and keep the same window, though
    return *this;
}


World::~World()
{

}

void World::step() {
    entities->updateTrackedEntitiesInSystems();

    pipeline->executeSystems();
}

void World::redraw()
{
    //window->clear(sf::Color::Black);
    //window->display();
}

bool World::shouldExitProgram() const
{
    return shouldExit;
}

void World::exitProgram() {
    shouldExit = true;
}

std::shared_ptr<Entities> const& World::getEntities() const
{
    return entities;
}

std::shared_ptr<sf::RenderWindow> const& World::getWindow() {
    return window;
}

ns const& World::getTickDuration() const {
    return tickDuration;
}
void World::setTickDuration(const ns duration) {
    tickDuration = duration;
}

std::shared_ptr<Pipeline> const &World::getPipeline() const {
    return pipeline;
}

void World::setPipeline(std::shared_ptr<Pipeline> const &ptr) {
    pipeline = ptr;
}
