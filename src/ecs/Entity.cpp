
/*-----------------------------------------
* @file Entity.cpp
* @brief
* @author Paul J
* @date Created: 22-04-2014
* @date Last Modified: Wed 10 Sep 2014 09:30:34 PM PDT
*-----------------------------------------*/

#include <typeindex>
#include "ecs/Entity.h"

Entity::~Entity() {

}

bool Entity::operator==(const Entity &other) const {
    return id == other.id;
}

bool Entity::hasComponent(std::type_index componentType) const {
    return (components.find(componentType) != components.end());
}


entity_id Entity::getId() {
    return id;
}

Entity::Entity(Entities& entities, entity_id eid)
        : enclosingEntityTracker(entities)
{
    id = eid;
}

Entity::Entity(const Entity &other) : enclosingEntityTracker(other.enclosingEntityTracker) {
    id = other.id;

}

Entity &Entity::operator=(const Entity &other) {
    components = other.components;
    id = other.id;
    enclosingEntityTracker = other.enclosingEntityTracker;
    isDeleted = other.isDeleted;

    return *this;
}
