
/*-----------------------------------------
* @file EntityUpdates.cpp
* @brief
* @author Paul J
* @date Created: 19-07-2014
* @date Last Modified: Sat 19 Jul 2014 04:22:38 AM PDT
*-----------------------------------------*/

#include <vector>
#include <algorithm>
#include "ecs/EntityUpdates.h"

EntityUpdates::EntityUpdates() { }

EntityUpdates::~EntityUpdates() { }

EntityUpdates::EntityUpdates(const EntityUpdates& other)
{
    updatedEntityIds = other.updatedEntityIds;
}

void EntityUpdates::clear()
{
    updatedEntityIds.clear();
}

EntityUpdates& EntityUpdates::operator=(const EntityUpdates& other)
{
    updatedEntityIds = other.updatedEntityIds;
    return *this;
}

void EntityUpdates::addUpdatedEntity(entity_id id)
{
    //We want the uniqueness constraint of a set, but the number of entities updated per
    // frame should be low enough that the quick traversal of a vector makes it more worthwhile.
    // Keep in mind that this function REQUIRES that the vector stays sorted
    bool alreadyAdded = std::binary_search(updatedEntityIds.begin(), updatedEntityIds.end(), id);

    //If we do add a new element, make sure we don't ruin the sorted vector order
    if (!alreadyAdded) {
        auto insertionPoint = std::lower_bound(updatedEntityIds.begin(), updatedEntityIds.end(), id);
        updatedEntityIds.insert(insertionPoint, id);
    }
}

const std::vector<entity_id>& EntityUpdates::getUpdatedEntityIds()
{
    return updatedEntityIds;
}



