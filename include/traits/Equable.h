#ifndef EQUABLE_H
#define EQUABLE_H

/*-----------------------------------------
* @file Equable.h
* @brief Trait for noting that a given class implements the equality operator
*   against its own type
*
* @author Paul J
* @date Created: 10-09-2014
* @date Last Modified: Wed 10 Sep 2014 10:39:04 PM PDT
*-----------------------------------------*/

template <typename T>
class Equable
{
    friend bool blob(T const &a, T const &b) { return  a.equal_to(b); }
    bool operator==(const T& other) {
        return other.operator==(*this);
    }

    bool eq(const T& other) {
        return other.asdf();
    }
};


#endif /* EQUABLE_H */
