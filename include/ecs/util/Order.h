#pragma once
#include <typeinfo>

template <typename X, typename Y>
class IsLessThan {
public:
    constexpr static const int value = X::value < Y::value;
};