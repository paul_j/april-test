#include <type_traits>
#include <vector>
#include <boost/fusion/container/set.hpp>
#include <boost/fusion/include/set.hpp>
#include <boost/fusion/include/set_fwd.hpp>
#include <cstddef>
#include <tuple>
#include <type_traits>
#include <utility>

namespace classSorting{

    template<typename A, typename B>
    struct min : std::conditional<std::decay<A>::type::value < std::decay<B>::type::value, A, B> {};

    template<typename A, typename B>
    struct max : std::conditional<std::decay<A>::type::value < std::decay<B>::type::value, B, A> {};

    template<typename ...N>
    struct list {
        using type = list<N...>;
        public:
            std::tuple<N...> value;
            list(N... params) { value = std::make_tuple(params...); }
    };

    template<typename Head, typename Tail>
    struct prepend;

    template<typename Head, typename... Tail>
    struct prepend<Head, list<Tail...>> : list<Head, Tail...> {};

    template<typename Head, typename ...Tail>
    struct insert_impl : list<Head> {};

    template<typename First, typename Next, typename ...Tail>
    struct insert_impl<First, Next, Tail...> :
            prepend<typename min<First,Next>::type,
                    typename insert_impl<
                            typename max<First,Next>::type,
                            Tail...>::type> {};


    template<typename Head, typename Tail>
    struct insert;

    template<typename Head, typename... Tail>
    struct insert<Head, list<Tail...>> : insert_impl<Head, Tail...> {};

    template<typename TypeList>
    struct sort : list<> {};

    template<typename Head, typename... Tail>
    struct sort<list<Head,Tail...>> : insert<Head, typename sort<list<Tail...>>::type> {};

    template<template<typename...> class Container, typename...Params>
    struct unordered_type { typedef Container<typename sort<list<Params...>>::type> type; };

    template<template<typename...> class T, typename... Tail>
    struct construct;

    template<template<typename...> class T, typename... Tail>
    struct construct<T, list<Tail...>> { typedef T<Tail...> type; };

    template<template<typename...> class T, typename...Types>
    struct unordered : construct<T, typename sort<list<Types...>>::type> {};

    template<template<typename...> class T, typename...Types>
    using unordered_t = typename unordered<T, Types...>::type;






    template<template<typename...> class T = std::tuple,  typename... Params, size_t... index>
    unordered_t<T, Params...> make_unordered_index_helper(std::tuple<Params&&...> argTuple, std::index_sequence<index...>) {
        return unordered_t<T, Params...>(
                std::get<
                        typename std::tuple_element<index,  unordered_t<std::tuple, Params&&...> >::type
                >(argTuple)...
        );
    }

    template<template<typename...> class T = std::tuple, typename... Params>
    unordered_t<T, Params...> make_unordered_tupleargs(std::tuple<Params...>&& argTuple) {
        constexpr auto Size = std::tuple_size<unordered_t<std::tuple, Params...>>::value;
        return make_unordered_index_helper<T, Params...>(argTuple, std::make_index_sequence<Size>());
    }

    template<template<typename...> class T = std::tuple, typename... Params>
    unordered_t<T, Params...> make_unordered(Params&&... args) {
        return make_unordered_tupleargs(std::forward_as_tuple(args...));
    }

};