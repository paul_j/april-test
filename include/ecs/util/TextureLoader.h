#ifndef TEXTURE_LOADER_H
#define TEXTURE_LOADER_H

#include <unordered_map>
#include <string>
#include <rapidjson/document.h>
#include "SFML/Window.hpp"
#include "SFML/Graphics.hpp"

typedef const std::string texture_handle;

class TextureLoader {
public:
    TextureLoader();
    ~TextureLoader();

    void loadTexture(texture_handle textureHandle);
    void loadTexture(const char* const textureId);
    const bool hasTexture(texture_handle textureHandle);
    const sf::Texture& getTexture(texture_handle textureHandle);
    void removeTexture(texture_handle textureHandle);
    void clear();
private:
    std::unordered_map<texture_handle, sf::Texture, std::hash<std::string>> textures;
    rapidjson::Document textureJson;
    void addTextureToMap(const std::string& textureId, const rapidjson::Value& entry);
    char readBuffer[65536];
    FILE* textureFile = NULL;
    const static sf::Texture INVALID_TEXTURE;
};

#endif /* TEXTURE_LOADER_H */