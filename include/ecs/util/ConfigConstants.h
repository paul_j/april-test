#ifndef CONFIG_CONSTANTS_H
#define CONFIG_CONSTANTS_H

#include <string>

namespace ecos {
    constexpr const char* BASE_DIR = "/home/paul/Code/side-projects/design_doc_game/";
    constexpr const char* CONFIG_DIR = "config/";
    constexpr const char* ASSET_DIR = "assets/";
    constexpr const char* TEXTURE_FILENAME = "textures.json";
};

#endif