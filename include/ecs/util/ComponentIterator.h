#ifndef COMPONENT_ITERATOR_H
#define COMPONENT_ITERATOR_H

#include <algorithm>
#include <iostream>
#include <array>
#include <tuple>
#include <utility>
#include <vector>
#include "ecs/EntityID.h"
#include <g2log/src/g2log.h>

template <typename... Ts>
class ComponentIterator {
    static constexpr std::size_t size = sizeof...(Ts);
    static constexpr auto indexSequence = std::index_sequence_for<Ts...>();

public:
    ComponentIterator() : ComponentIterator(indexSequence) { }

    template<size_t... Is>
    ComponentIterator(std::index_sequence<Is...>) : vals(), indices(), finished(false), sizes({static_cast<size_t>(std::get<Is>(componentVectors).size())...}) {
        if (std::any_of(std::begin(sizes), std::end(sizes), [](size_t s) { return s == 0; })) {
            finished = true;
        } else {
            moveToNext(indexSequence);
        }
    }

    std::tuple<Ts&...> get()
    {
        return get(indexSequence);
    }

    ComponentIterator& operator ++()
    {
        moveToNext(indexSequence);
        return *this;
    }

    bool isFinished() const {
        return finished;
    }
private:
    template <std::size_t...Is>
    std::tuple<Ts&...> get(std::index_sequence<Is...>)
    {
        return std::forward_as_tuple(std::get<Is>(componentVectors)[indices[Is]]...);
    }

    template <std::size_t...Is>
    void moveToNext(std::index_sequence<Is...>) {
        bool allEqual;

        if (isFinished()) return;
        for (size_t& index : indices) index++;
        while(true)
        {
            bool is_ended[size] = {indices[Is] == std::get<Is>(sizes)...};
            if (std::any_of(std::begin(is_ended), std::end(is_ended), [](bool b) { return b; })) {
                finished = true;
                return;
            }

            std::tuple<Ts&...> components = std::tie(std::get<Is>(componentVectors)[indices[Is]]...);
            std::array<entity_id, size> entityIds = {std::get<Is>(components).getEntityId()...};

            allEqual = true;
            size_t minIndex = 0;
            entity_id minId = entityIds[0];
            for (size_t i = 1; i < size; i++) {
                entity_id otherId = entityIds[i];
                if (otherId != minId) {
                    allEqual = false;
                    if (otherId < minId) {
                        minId = otherId;
                        minIndex = i;
                    }
                }
            }

            if (allEqual) {
                return;
            } else {
                indices[minIndex]++;
            }
        }
    }

    std::tuple<std::vector<Ts>&...> componentVectors = std::tie(Ts::getSortedComponents()...);
    std::tuple<Ts* ...> vals;
    std::array<std::size_t, size> indices;
    bool finished;
    std::array<size_t, size> sizes;
};


#endif /* COMPONENT_ITERATOR_H */