#ifndef ECS_UNITS_H
#define ECS_UNITS_H

#include <boost/units/unit.hpp>
#include <boost/units/quantity.hpp>
#include <boost/units/systems/si/time.hpp>
#include <boost/units/systems/si/length.hpp>
#include <boost/units/systems/si/velocity.hpp>
#include <boost/units/systems/si/acceleration.hpp>
#include <boost/units/systems/si/mass.hpp>
#include <boost/units/systems/si/prefixes.hpp>
#include <boost/units/systems/cgs.hpp>


namespace units {
    typedef boost::units::quantity<boost::units::si::time> time_type;
    typedef boost::units::quantity<boost::units::si::mass> mass_type;
    typedef boost::units::quantity<boost::units::si::length> length_type;
    typedef boost::units::quantity<boost::units::si::velocity> vel_type;
    typedef boost::units::quantity<boost::units::si::acceleration> accel_type;

    using boost::units::si::milli;
    using boost::units::si::centi;
    using boost::units::si::kilo;
    using boost::units::si::nano;
    using boost::units::si::second;
    using boost::units::si::kilogram;
    using boost::units::si::meter;
    using boost::units::si::meter_per_second;
    using boost::units::si::meter_per_second_squared;
    using boost::units::cgs::gram;
}


#endif /* ECS_UNITS_H */