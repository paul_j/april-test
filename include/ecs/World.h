#ifndef WORLD_H
#define WORLD_H

/*-----------------------------------------
* @file World.h
* @briefo
* @author Paul J
* @date Created: 17-07-2014
* @date Last Modified: Wed 10 Sep 2014 08:10:09 PM PDT
*-----------------------------------------*/

#include <vector>
#include <cstdio>
#include <memory>
#include <chrono>
#include "SFML/Window.hpp"
#include "SFML/Graphics.hpp"
#include "config/Typedefs.h"
#include "util/ComponentIterator.h"

class System;
class Entities;
class Pipeline;

class World
{
public:
    World();
    World(const World& other);
    World& operator= (const World& other);
    virtual ~World();
    void step();
    void redraw();
    bool shouldExitProgram() const;
    void exitProgram();
    std::shared_ptr<Entities> const& getEntities() const;
    std::shared_ptr<Pipeline> const& getPipeline() const;
    void setPipeline(std::shared_ptr<Pipeline> const&);
    std::shared_ptr<sf::RenderWindow> const& getWindow();
    ns const& getTickDuration() const;
    void setTickDuration(const ns duration);
private:
    std::shared_ptr<Entities> entities = nullptr;
    std::shared_ptr<Pipeline> pipeline = nullptr;
    std::shared_ptr<sf::RenderWindow> window = nullptr;
    ns tickDuration;
    bool shouldExit = false;
};


#endif /* WORLD_H */
