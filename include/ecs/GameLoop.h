#ifndef GAMELOOP_H
#define GAMELOOP_H

/*-----------------------------------------
* @file GameLoop.h
* @brief
* @author Paul J
* @date Created: 19-07-2014
* @date Last Modified: Tue 22 Jul 2014 11:27:22 PM PDT
*-----------------------------------------*/

#include "ecs/World.h"
#include "SFML/System.hpp"
#include "SFML/Window.hpp"
#include "SFML/Graphics.hpp"

class GameLoop
{
public:
    GameLoop();
    GameLoop(double desiredFps);
    GameLoop(const GameLoop& other);
    virtual ~GameLoop();
    void start();
private:
    World world;
    double maxFps;
};


#endif /* GAMELOOP_H */
