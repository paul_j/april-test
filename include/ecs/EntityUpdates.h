#ifndef ENTITYUPDATES_H
#define ENTITYUPDATES_H

/*-----------------------------------------
* @file EntityUpdates.h
* @brief
* @author Paul J
* @date Created: 19-07-2014
* @date Last Modified: Sat 19 Jul 2014 04:08:41 AM PDT
*-----------------------------------------*/

#include <vector>
#include "ecs/EntityID.h"

class EntityUpdates
{
public:
    EntityUpdates();
    EntityUpdates(const EntityUpdates& other);
    EntityUpdates& operator= (const EntityUpdates& other);
    virtual ~EntityUpdates();
    const std::vector<entity_id>& getUpdatedEntityIds();
    void addUpdatedEntity(entity_id id);
    void clear();
private:
    std::vector<entity_id> updatedEntityIds;
};


#endif /* ENTITYUPDATES_H */
