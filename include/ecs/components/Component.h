#pragma once    
#ifndef COMPONENT_H
#define COMPONENT_H

/*-----------------------------------------
* @file Component.h
* @brief
* @author Paul J
* @date Created: 17-04-2014
* @date Last Modified: Wed 10 Sep 2014 09:33:13 PM PDT
*-----------------------------------------*/

#include <string>
#include "ecs/EntityID.h"

typedef unsigned int component_id;

/* TODO: Super-components
    e.g. If a given entity has a SizeComponent, WeightComponent,
     VelocityComponent, and PositionComponent, then it fulfills the
     contract of a PhysicalObjectComponent, and can be recognized as such.
     Determine if this is worthwhile, and scope out if so.
 */

class Component
{
public:
    Component(const component_id cid, const entity_id eid) : componentId(cid), entityId(eid) {}
    Component(const Component& other) : componentId(other.componentId), entityId(other.entityId) {}
    virtual ~Component() {}
    Component& operator=(const Component& other) {
        componentId = other.componentId;
        entityId = other.entityId;
        return *this;
    }

    const component_id getComponentId() const { return componentId; }
    const entity_id getEntityId() const { return entityId; }
protected:
    component_id componentId;
    entity_id entityId;
};

#endif /* COMPONENT_H */
