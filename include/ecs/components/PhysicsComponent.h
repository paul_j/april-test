#ifndef PHYSICS_COMPONENT_H
#define PHYSICS_COMPONENT_H

/*-----------------------------------------
* @file PhysicsComponent.h
* @brief
* @author Paul J
* @date Created: 21-11-2014
* @date Last Modified:
*-----------------------------------------*/

#include "ecs/components/ConcreteComponent.h"
#include <bitset>
#include "ecs/util/Units.h"

using namespace units;

class PhysicsComponent : public ConcreteComponent<PhysicsComponent> {
public:
    ~PhysicsComponent() = default;
    PhysicsComponent(const PhysicsComponent& other);
    static const component_id create(const std::pair<vel_type, vel_type> velocity,
                                     const mass_type mass,
                                     const entity_id parentEntity);

    std::pair<vel_type, vel_type> const& getVelocity() const;
    void setVelocity(const std::pair<vel_type, vel_type> velocity);
    void addVelocity(const std::pair<vel_type, vel_type> velocity);
    mass_type const& getMass() const;
    void setMass(const mass_type mass);
    bool operator==(const PhysicsComponent& other) const;
    void setMoveable(bool moveable);
    bool isMoveable() const;
    void setCollidable(bool collidable);
    bool isCollidable() const;
private:
    PhysicsComponent(const std::pair<vel_type, vel_type> velocity,
                     const mass_type mass,
                     const component_id id,
                     const entity_id parentEntity);

    PhysicsComponent(const std::pair<vel_type, vel_type> velocity,
                     const mass_type mass,
                     bool collidable,
                     bool moveable,
                     const component_id id,
                     const entity_id parentEntity);

    std::pair<vel_type, vel_type> velocity;
    mass_type mass;
    std::bitset<2> flags;
};

#endif /* PHYSICS_COMPONENT_H */

