#include "Component.h"

#ifndef MOVEMENT_CONFIGURATION_H
#define MOVEMENT_CONFIGURATION_H

/*-----------------------------------------
* @file MovementConfigurationComponent.h
* @brief
* @author Paul J
* @date Created: 21-11-2014
* @date Last Modified:
*-----------------------------------------*/

#include "ecs/components/ConcreteComponent.h"
#include "ecs/util/Units.h"

using namespace units;

class MovementConfigurationComponent : public ConcreteComponent<MovementConfigurationComponent> {
public:
    virtual ~MovementConfigurationComponent();
    MovementConfigurationComponent(const MovementConfigurationComponent& other);
    static const component_id create(const std::pair<accel_type, accel_type> acceleration,
                                     const std::pair<vel_type, vel_type> maxVelocity,
                                     const entity_id parentEntity);
    std::pair<accel_type, accel_type> const& getAcceleration() const;
    void setAcceleration(const std::pair<accel_type, accel_type> acceleration);
    std::pair<vel_type, vel_type> const& getMaxVelocity() const;
    void setMaxVelocity(const std::pair<vel_type, vel_type> maxVelocity);
    bool operator==(const MovementConfigurationComponent& other) const;
private:
    std::pair<accel_type, accel_type> acceleration;
    std::pair<vel_type, vel_type> maxVelocity;

    MovementConfigurationComponent(const std::pair<accel_type, accel_type> acceleration,
                                    const std::pair<vel_type, vel_type> maxVelocity,
                                    const component_id id,
                                    const entity_id parentEntity);
};

#endif /* MOVEMENT_CONFIGURATION_H */
