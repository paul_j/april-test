#ifndef KEYBOARD_AFFECTED_H
#define KEYBOARD_AFFECTED_H

/*-----------------------------------------
* @file KeyboardAffectedComponent.h
* @brief
* @author Paul J
* @date Created: 28-08-2014
* @date Last Modified: Wed 10 Sep 2014 10:46:10 PM PDT
*-----------------------------------------*/

#include <memory>
#include "ecs/components/ConcreteComponent.h"

using namespace std;

class KeyboardAffectedComponent : public ConcreteComponent<KeyboardAffectedComponent>
{
public:
    virtual ~KeyboardAffectedComponent();
    KeyboardAffectedComponent(const component_id id, const entity_id parentEntity);
    static const component_id create(const entity_id parentEntity);
};

#endif /* KEYBOARD_AFFECTED_H */
