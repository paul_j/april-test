#ifndef POSITION_COMPONENT_H
#define POSITION_COMPONENT_H

/*-----------------------------------------
* @file PositionComponent.h
* @brief
* @author Paul J
* @date Created: 21-11-2014
* @date Last Modified:
*-----------------------------------------*/

#include "ecs/components/ConcreteComponent.h"

class PositionComponent : public ConcreteComponent<PositionComponent> {
public:
    virtual ~PositionComponent();
    PositionComponent(const PositionComponent& other);
    static const component_id create(std::pair<double, double> position, const entity_id parentEntity);

    std::pair<double, double> const& getPosition() const;
    std::pair<double, double>& getPositionRef();
    void setPosition(const std::pair<double, double> position);
    void setPosition(const double x, const double y);
    void addOffset(const std::pair<double, double> offset);
    void addOffset(const double x, const double y);
    bool operator==(const PositionComponent& other) const;
private:
    std::pair<double, double> position;

    PositionComponent(std::pair<double, double> position, const component_id id, const entity_id parentEntity);
};

#endif /* POSITION_COMPONENT_H */

