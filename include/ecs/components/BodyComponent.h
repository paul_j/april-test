#ifndef BODY_COMPONENT_H
#define BODY_COMPONENT_H

/*-----------------------------------------
* @file BodyComponent.h
* @brief
* @author Paul J
* @date Created: 12-04-2015
* @date Last Modified:
*-----------------------------------------*/

#include "ecs/components/ConcreteComponent.h"
#include "ecs/util/Units.h"

using namespace units;

class BodyComponent : public ConcreteComponent<BodyComponent> {
public:
    virtual ~BodyComponent() = default;
    BodyComponent(const BodyComponent& other);
    static const component_id create(const std::pair<length_type, length_type> position,
                                     const std::pair<length_type, length_type> dimensions,
                                     const entity_id parentEntity);

    static const component_id create(const std::pair<length_type, length_type> position,
                                     const std::pair<length_type, length_type> dimensions,
                                     const std::pair<vel_type, vel_type> velocity,
                                     const mass_type mass,
                                     bool isMoveable,
                                     bool isCollidable,
                                     const entity_id parentEntity);

    std::pair<length_type, length_type> const& getPosition() const;
    void setPosition(const std::pair<length_type, length_type> position);
    void setPosition(const length_type x, const length_type y);
    void move(const std::pair<length_type, length_type> offset);
    void move(const length_type x, const length_type y);

    std::pair<length_type, length_type> const& getDimensions() const;
    void setDimensions(const std::pair<length_type, length_type> position);
    void setDimensions(const length_type width, const length_type height);

    std::pair<vel_type, vel_type> const& getVelocity() const;
    void setVelocity(const std::pair<vel_type, vel_type> velocity);
    void setVelocity(const vel_type xVel, const vel_type yVel);

    mass_type const& getMass() const;
    void setMass(const mass_type mass);

    bool isMoveable() const;
    void setMoveable(bool moveable);

    bool isCollidable() const;
    void setCollidable(bool collidable);

    bool operator==(const BodyComponent& other) const;
private:
    std::pair<length_type, length_type> position;
    std::pair<length_type, length_type> dimensions;
    std::pair<vel_type, vel_type> velocity;
    mass_type mass;

    bool moveable;
    bool collidable;

    BodyComponent(const std::pair<length_type, length_type> position,
                  const std::pair<length_type, length_type> dimensions,
                  const std::pair<vel_type, vel_type> velocity,
                  const mass_type mass,
                  bool isMoveable,
                  bool isCollidable,
                  const component_id id,
                  const entity_id parentEntity);
};



#endif /* BODY_COMPONENT_H */
