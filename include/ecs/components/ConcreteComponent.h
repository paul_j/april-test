#ifndef CONCRETE_COMPONENT_H
#define CONCRETE_COMPONENT_H

#include <vector>
#include <assert.h>
#include <algorithm>
#include "ecs/components/Component.h"
#include "ecs/util/ComponentIterator.h"

template <typename T> class ConcreteComponent : public Component {
public:
    template <typename... Types> friend class ComponentIterator;

    ConcreteComponent(const component_id cid, const entity_id eid ) : Component(cid, eid) {}
    virtual ~ConcreteComponent() {}

    inline static std::vector<T>& getSortedComponents() {
        return sortedComponents;
    }
protected:
    static std::vector<T> sortedComponents;
    static std::vector<component_id> freeIndices;

    static component_id nextIndex;
    static component_id getFreeId();
    static void storeComponent(const T& component);
};

template <typename T> component_id ConcreteComponent<T>::nextIndex;
template <typename T> std::vector<T> ConcreteComponent<T>::sortedComponents;
template <typename T> std::vector<component_id> ConcreteComponent<T>::freeIndices;

template <typename T> component_id ConcreteComponent<T>::getFreeId() {
   return nextIndex++;
}

template <typename T> void ConcreteComponent<T>::storeComponent(const T& component)
{
    auto insertLocation = std::lower_bound(sortedComponents.begin(), sortedComponents.end(), component.getEntityId(),
            [](const T& checkedComponent, const entity_id toInsert){ return checkedComponent.getEntityId() < toInsert; });
    sortedComponents.insert(insertLocation, component);
}

#endif /* CONCRETE_COMPONENT_H */