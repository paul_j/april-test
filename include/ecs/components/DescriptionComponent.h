#ifndef DESCRIPTIONCOMPONENT_H
#define DESCRIPTIONCOMPONENT_H

/*-----------------------------------------
* @file DescriptionComponent.h
* @brief Component containing a description of its entity
* @author Paul J
* @date Created: 16-04-2014
* @date Last Modified: Wed 10 Sep 2014 10:46:06 PM PDT
*-----------------------------------------*/

#include "ecs/components/ConcreteComponent.h"
#include "traits/Equable.h"
#include <string>

using namespace std;

class DescriptionComponent : public ConcreteComponent<DescriptionComponent> {
public:
    virtual ~DescriptionComponent();
    DescriptionComponent(const DescriptionComponent& other);
    static const component_id create(const string& _descriptionText, const entity_id parentEntity);

    bool operator==(const DescriptionComponent& other) const;
    const string& getDescriptionText() const;
    void setDescriptionText(const string& _descriptionText);
private:
    string descriptionText;

    DescriptionComponent(const string& _descriptionText, const component_id id, const entity_id parentEntity);
};


#endif /* DESCRIPTIONCOMPONENT_H */
