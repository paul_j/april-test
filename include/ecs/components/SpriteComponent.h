#ifndef SPRITE_COMPONENT_H
#define SPRITE_COMPONENT_H

/*-----------------------------------------
* @file SpriteComponent.h
* @brief Component containing a drawable representation of an entity
* @author Paul J
* @date Created: 30-11-2014
* @date Last Modified:
*-----------------------------------------*/

#include "ecs/components/ConcreteComponent.h"
#include <string>
#include <functional>
#include "SFML/Window.hpp"
#include "SFML/Graphics.hpp"

using namespace std;

class SpriteComponent : public ConcreteComponent<SpriteComponent> {
public:
    virtual ~SpriteComponent();
    SpriteComponent(const SpriteComponent &other);
    static const component_id create(const sf::Sprite& sprite, const entity_id parentEntity);
    static const component_id create(const sf::Texture& texture,
                                     const int widthInPixels,
                                     const int heightInPixels,
                                     const entity_id parentEntity);

    bool operator==(const SpriteComponent &other) const;
    sf::Sprite& getSpriteRef();
    void setSprite(sf::Sprite sprite);
    void setTexture(const sf::Texture& texture);
    void setSpriteSize(const int width, const int height);
private:
    sf::Sprite sprite;

    SpriteComponent(sf::Sprite sprite, const component_id id, const entity_id parentEntity);
    SpriteComponent(const sf::Texture &texture,
                    const int widthInPixels,
                    const int heightInPixels,
                    const component_id id,
                    const entity_id parentEntity);
};


#endif /* SPRITE_COMPONENT_H */
