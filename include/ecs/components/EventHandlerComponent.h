#ifndef EventHandlerComponent_H
#define EventHandlerComponent_H

/*-----------------------------------------
* @file EventHandlerComponent.h
* @brief
* @author Paul J
* @date Created: 28-08-2014
* @date Last Modified: Wed 10 Sep 2014 10:46:10 PM PDT
*-----------------------------------------*/

#include "ecs/components/ConcreteComponent.h"
#include "traits/Equable.h"
#include <string>
#include <vector>
#include <memory>
#include <unordered_map>
#include "SFML/Window.hpp"

class EventHandlerComponent : public ConcreteComponent<EventHandlerComponent>
{
public:
    virtual ~EventHandlerComponent();
    EventHandlerComponent(const component_id id, const entity_id parentEntity);
    static const component_id create(const entity_id parentEntity);

    std::shared_ptr<std::unordered_map<sf::Event::EventType, std::vector<sf::Event>, std::hash<int> >> events;
};


#endif /* EventHandlerComponent_H */
