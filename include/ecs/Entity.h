#ifndef ENTITY_H
#define ENTITY_H

/*-----------------------------------------
* @file Entity.h
* @brief
* @author Paul J
* @date Created: 17-04-2014
* @date Last Modified: Wed 10 Sep 2014 09:08:51 PM PDT
*-----------------------------------------*/

#include <unordered_map>
#include <memory>
#include <typeindex>
#include <algorithm>
#include <vector>
#include "ecs/components/Component.h"
#include "ecs/EntityUpdates.h"
#include "ecs/EntityID.h"
#include "ecs/Entities.h"

class Entity
{
friend class Entities;
public:
    Entity(Entities& entities, entity_id eid);
    Entity(const Entity& other);
    virtual ~Entity();

    Entity& operator=(const Entity& other);
    bool operator== (const Entity& other) const;
    template<typename T, typename... Args> void setComponent(Args&& ...);
    template<typename T> void removeComponent();
    template<typename T> bool hasComponent() const;
    bool hasComponent(const std::type_index componentType) const;

    entity_id getId();
private:
    /** A collection of components indexed by their type */
    std::unordered_map<std::type_index, component_id> components;
    entity_id id;
    Entities& enclosingEntityTracker;
    bool isDeleted = false;
};

template<typename T, typename... Args>
void Entity::setComponent(Args&& ... args) {
    //TODO: Assert that T is a valid Component type
    component_id id = T::create(std::forward<Args>(args) ..., getId());
    components[std::type_index(typeid(T))] = id;
}

template<typename T>
void Entity::removeComponent() {
    auto& sortedComponents = T::getSortedComponents();
    entity_id thisId = getId();

    auto insertLocation = std::lower_bound(sortedComponents.begin(), sortedComponents.end(), thisId,
            [](const T& checkedComponent, const entity_id thisId){ return checkedComponent.getEntityId() < thisId; });
    sortedComponents.erase(insertLocation);
}

template<typename T>
bool Entity::hasComponent() const {
    return (components.find(std::type_index(typeid(T))) != components.end());
}

#endif /* ENTITY_H */
