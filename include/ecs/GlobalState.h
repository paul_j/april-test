#ifndef GLOBAL_STATE_H
#define GLOBAL_STATE_H

#include "util/TextureLoader.h"
#include "ecs/Scene.h"

namespace ecos {
    extern TextureLoader textureLoader;
    extern Scene sceneView;
}

#endif /* GLOBAL_STATE_H */