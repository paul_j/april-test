#ifndef ENTITYID_H
#define ENTITYID_H

/*-----------------------------------------
* @file EntityID.h
* @brief
* @author Paul J
* @date Created: 24-04-2014
* @date Last Modified: Thu 24 Apr 2014 12:28:25 AM PDT
*-----------------------------------------*/

typedef unsigned int entity_id;

#endif /* ENTITYID_H */
