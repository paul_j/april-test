#ifndef ENTITIES_H
#define ENTITIES_H

/*-----------------------------------------
* @file Entities.h
* @brief
* @author Paul J
* @date Created: 17-04-2014
* @date Last Modified: Tue 26 Aug 2014 07:51:37 PM PDT
*-----------------------------------------*/

class Entity;
class World;
class System;

#include <vector>
#include <unordered_map>
#include <unordered_set>
#include <memory>
#include "ecs/components/Component.h"
#include "ecs/Entity.h"
#include "ecs/EntityUpdates.h"

//Actual storage of entity objects. Pointers are vended out
class Entities
{
public:
    Entities(World& world);
    Entities(const Entities& other);
    Entities& operator= (const Entities& other);
    virtual ~Entities();
    Entity& get(entity_id id);
    Entity& operator[](entity_id id);
    Entity& createEntity();
    void deleteEntity(entity_id id);
    void updateTrackedEntitiesInSystems();
private:
    EntityUpdates modifiedEntities;
    World& referencedWorld;
    entity_id nextEntityId;
    std::vector<entity_id> freedEntityIds;
    std::vector<Entity> entities;

    const entity_id getFreeEntityId();
};


#endif /* ENTITIES_H */
