#ifndef EventPopulationSystem_H
#define EventPopulationSystem_H

/*-----------------------------------------
* @file EventPopulationSystem.h
* @brief
* @author Paul J
* @date Created: 28-08-2014
* @date Last Modified: Thu 28 Aug 2014 10:48:47 PM PDT
*-----------------------------------------*/

#include "ecs/systems/System.h"

class EventPopulationSystem : public System
{
public:
    EventPopulationSystem(World& world);
    EventPopulationSystem(const EventPopulationSystem& other);
    virtual ~EventPopulationSystem();
    void execute();
private:

};


#endif /* EventPopulationSystem_H */
