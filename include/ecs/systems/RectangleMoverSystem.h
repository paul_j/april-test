#ifndef RECTANGLE_MOVER_SYSTEM_H
#define RECTANGLE_MOVER_SYSTEM_H

/*-----------------------------------------
* @file RectangleMoverSystem.h
* @brief
* @author Paul J
* @date Created: 21-11-2014
* @date Last Modified:
*-----------------------------------------*/

#include "ecs/systems/System.h"

class RectangleMoverSystem : public System {
public:
    RectangleMoverSystem(World& world);
    RectangleMoverSystem(const RectangleMoverSystem& other);
    virtual ~RectangleMoverSystem();
    void execute();
private:
};

#endif /* RECTANGLE_MOVER_SYSTEM_H */