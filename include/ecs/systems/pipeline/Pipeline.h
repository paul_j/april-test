#ifndef PIPELINE_H
#define PIPELINE_H

/**
* Defines the execution order of Systems, allowing a single
* entry point to resolve system execution with each system
* knowing that all dependency systems have been run.
*/

#include <typeindex>
#include "ecs/systems/System.h"
#include <vector>

class Pipeline {
public:
    Pipeline() {};
    virtual ~Pipeline() {};

    const virtual void executeSystems() = 0;
    virtual void addSystem(std::shared_ptr<System> const system) = 0;
    virtual void addSystem(std::shared_ptr<System> const, const std::initializer_list<std::type_index> dependencies) = 0;
    virtual const std::vector<std::shared_ptr<System>>& getSystems() = 0;
};

#endif /* PIPELINE_H */