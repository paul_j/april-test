#ifndef SERIAL_PIPELINE_H
#define SERIAL_PIPELINE_H

#include "ecs/systems/pipeline/Pipeline.h"

/**
* Defines the execution order of Systems, allowing a single
* entry point to resolve system execution with each system
* knowing that all dependency systems have been run.
*/
class SerialPipeline : public Pipeline {
public:
    SerialPipeline();
    virtual ~SerialPipeline();

    const void executeSystems();
    void addSystem(std::shared_ptr<System> const);
    void addSystem(std::shared_ptr<System> const, const std::initializer_list<std::type_index> dependencies);
    const std::vector<std::shared_ptr<System>>& getSystems();
private:
    std::vector<std::shared_ptr<System>> systems;
};

#endif /* SERIAL_PIPELINE_H */
