#ifndef ASYNC_GROUP_PIPELINE_H
#define ASYNC_GROUP_PIPELINE_H

#include "ecs/systems/pipeline/Pipeline.h"
#include "ecs/systems/pipeline/ThreadPool.h"

/**
* Executes systems in asynchronous groups, with each group
* requiring all systems to be completed before launching the next group
*/
class AsyncGroupPipeline : public Pipeline {
public:
    AsyncGroupPipeline();
    virtual ~AsyncGroupPipeline();

    const void executeSystems();
    void addSystem(std::shared_ptr<System> const);
    void addSystem(std::shared_ptr<System> const, std::initializer_list<std::type_index> dependencies);
    const std::vector<std::shared_ptr<System>>& getSystems();
private:
    std::vector<std::vector<std::shared_ptr<System>>> systemGroups;
    std::vector<std::shared_ptr<System>> allSystems;
    ThreadPool threadPool;
};

#endif /* ASYNC_GROUP_PIPELINE_H */