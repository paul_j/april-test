#ifndef THREAD_POOL_H
#define THREAD_POOL_H

#include <future>
#include <deque>
#include <vector>

class ThreadPool {
public:
    ThreadPool(const int size);
    ~ThreadPool();

    void execute(std::shared_future<void> deferredExecution);
    void wait();
private:
    std::vector<std::thread> threadPool;
    std::deque<std::shared_future<void>> tasks;
    bool shouldEnd;
    void processEvents();
};

#endif /* THREAD_POOL_H */


