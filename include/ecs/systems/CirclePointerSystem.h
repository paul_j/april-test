#ifndef CIRCLEPOINTERSYSTEM_H
#define CIRCLEPOINTERSYSTEM_H

/*-----------------------------------------
* @file CirclePointerSystem.h
* @brief
* @author Paul J
* @date Created: 10-09-2014
* @date Last Modified: Wed 10 Sep 2014 07:30:32 PM PDT
*-----------------------------------------*/

#include "ecs/systems/System.h"

class CirclePointerSystem : public System
{
public:
    CirclePointerSystem(World& world);
    CirclePointerSystem(const CirclePointerSystem& other);
    virtual ~CirclePointerSystem();
    void execute();
private:

};


#endif /* CIRCLEPOINTERSYSTEM_H */
