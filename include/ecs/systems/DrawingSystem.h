

#ifndef DRAWING_SYSTEM_H
#define DRAWING_SYSTEM_H


/*-----------------------------------------
* @file DrawingSystem.h
* @brief
* @author Paul J
* @date Created: 10-09-2014
* @date Last Modified: Wed 10 Sep 2014 07:30:32 PM PDT
*-----------------------------------------*/

#include "ecs/systems/System.h"

class DrawingSystem : public System
{
public:
    DrawingSystem(World& world);
    DrawingSystem(const DrawingSystem& other);
    virtual ~DrawingSystem();
    void execute();
    static const double getPixelsPerMeter();
    static void setPixelsPerMeter(const double ppm);
private:
    static double pixelsPerMeter;
};
#endif /* DRAWING_SYSTEM_H */
