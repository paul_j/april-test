#ifndef ECHOSYSTEM_H
#define ECHOSYSTEM_H

/*-----------------------------------------
* @file EchoSystem.h
* @brief
* @author Paul J
* @date Created: 28-08-2014
* @date Last Modified: Thu 28 Aug 2014 10:47:56 PM PDT
*-----------------------------------------*/

#include "ecs/systems/System.h"

class EchoSystem : public System
{
public:
    EchoSystem(World& world);
    EchoSystem(const EchoSystem& other);
    virtual ~EchoSystem();
    void execute();
};


#endif /* ECHOSYSTEM_H */
