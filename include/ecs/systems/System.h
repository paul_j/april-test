#ifndef SYSTEM_H
#define SYSTEM_H

/*-----------------------------------------
* @file System.h
* @brief
* @author Paul J
* @date Created: 18-04-2014
* @date Last Modified: Wed 10 Sep 2014 07:51:27 PM PDT
*-----------------------------------------*/

#include "ecs/Entity.h"
#include "ecs/components/Component.h"
#include "ecs/World.h"
#include <memory>
#include <typeindex>
#include <vector>
#include <mutex>

class World;
class Entities;

class System
{
public:
    System(World& world);
    System(const System& other);
    virtual ~System();
    virtual void execute() =0;
    void trackEntity(entity_id id);
    void untrackEntity(entity_id id);
    bool shouldTrack(entity_id id) const;
    bool shouldTrack(const Entity& entity) const;
    static std::mutex windowMutex;
protected:
    std::vector<std::type_index> requirements;
    std::vector<entity_id> trackedEntities;
    World& referencedWorld;
};

#endif /* SYSTEM_H */
