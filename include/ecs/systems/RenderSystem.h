#ifndef RENDER_SYSTEM_H
#define RENDER_SYSTEM_H
/*-----------------------------------------
* @file RenderSystem.h
* @brief
* @author Paul J
* @date Created: 12-10-2014
* @date
*-----------------------------------------*/

#include "ecs/systems/System.h"

class RenderSystem : public System
{
public:
    RenderSystem(World& world);
    RenderSystem(const RenderSystem& other);
    virtual ~RenderSystem();
    void execute();
private:
    sf::VertexArray vertexArray;
};
#endif /* RENDER_SYSTEM_H */