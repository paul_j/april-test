#ifndef GRAVITY_SYSTEM_H
#define GRAVITY_SYSTEM_H

#include "ecs/systems/System.h"

class GravitySystem : public System {
public:
    GravitySystem(World& world);
    GravitySystem(const GravitySystem& other);
    ~GravitySystem() = default;
    void execute();
};

#endif /* GRAVITY_SYSTEM_H */