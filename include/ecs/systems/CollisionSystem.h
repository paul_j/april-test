//
// Created by paul on 3/24/15.
//

#ifndef _DESIGNDOCGAME_COLLISIONSYSTEM_H_
#define _DESIGNDOCGAME_COLLISIONSYSTEM_H_

#include "ecs/systems/System.h"

class CollisionSystem : public System {
public:
    CollisionSystem(World& world);
    CollisionSystem(const CollisionSystem& other);
    ~CollisionSystem() = default;
    void execute();
};


#endif //_DESIGNDOCGAME_COLLISIONSYSTEM_H_
