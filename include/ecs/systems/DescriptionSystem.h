#ifndef DESCRIPTIONSYSTEM_H
#define DESCRIPTIONSYSTEM_H

/*-----------------------------------------
* @file DescriptionSystem.h
* @brief
* @author Paul J
* @date Created: 22-04-2014
* @date Last Modified: Thu 28 Aug 2014 10:47:30 PM PDT
*-----------------------------------------*/

#include "ecs/systems/System.h"

class DescriptionSystem : public System
{
public:
    DescriptionSystem(World& world);
    DescriptionSystem(const DescriptionSystem& other);
    virtual ~DescriptionSystem();
    void execute();
private:

};


#endif /* DESCRIPTIONSYSTEM_H */
