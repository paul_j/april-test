#ifndef TYPEDEFS_H
#define TYPEDEFS_H

#include <chrono>

typedef std::chrono::nanoseconds ns;
typedef std::chrono::duration<double> dSeconds;

#endif /* TYPEDEFS_H */