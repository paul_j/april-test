
/*-----------------------------------------
* @file WorldTest.cpp
* @brief
* @author Paul J
* @date Created: 17-07-2014
* @date Last Modified: Mon 08 Jun 2015 09:44:58 PM PDT
*-----------------------------------------*/

#include "gtest/gtest.h"
#include "ecs/World.h"
#include "ecs/Entities.h"
#include "ecs/components/DescriptionComponent.h"
#include "ecs/systems/DescriptionSystem.h"
#include <ecs/systems/pipeline/Pipeline.h>

class WorldTestFixture : public testing::Test {
protected:
    virtual void SetUp() {

    }

};

TEST_F(WorldTestFixture, TestThatWorldWithDescriptionsExecutesDescriptionSystem) {
    /*
    

    World tellMeAboutEverythingWorld;
    DescriptionSystem tellYouAboutEachThingSystem(tellMeAboutEverythingWorld);
    tellMeAboutEverythingWorld.getPipeline()->addSystem(std::shared_ptr<System>(&tellYouAboutEachThingSystem));

    const std::shared_ptr<Entities> entities = tellMeAboutEverythingWorld.getEntities();
    Entity entity = entities->createEntity();
    ASSERT_EQ(entity.getId(), 0);

    std::shared_ptr<DescriptionComponent> descriptionComponent = make_shared<DescriptionComponent>("TestText\n");
    ASSERT_EQ(descriptionComponent->getDescriptionText(), "TestText\n");

    entity.addComponent(descriptionComponent);
    ASSERT_EQ(entity.getComponent<DescriptionComponent>(), *descriptionComponent);
    ASSERT_TRUE(entity.hasComponent(DescriptionComponent::TYPE));

    ASSERT_EQ(entities->get(entity.getId()), entity);

    vector<std::shared_ptr<System>> expectedSystems;
    expectedSystems.push_back(std::shared_ptr<System>(&tellYouAboutEachThingSystem));
    ASSERT_EQ(tellMeAboutEverythingWorld.getPipeline()->getSystems(), expectedSystems);
    tellMeAboutEverythingWorld.step();
    */
    ASSERT_EQ(0,0);
}
